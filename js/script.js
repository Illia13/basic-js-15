"use strict";

document.getElementById("myButton").addEventListener("click", function () {
  setTimeout(function () {
    document.getElementById("myDiv").textContent = "Операція виконана успішно";
  }, 3000);
});

let countdownElement = document.getElementById("countdown");
let countdownValue = 10;

let interval = setInterval(function () {
  countdownValue--;
  if (countdownValue > 0) {
    countdownElement.textContent = countdownValue;
  } else {
    countdownElement.textContent = "Зворотній відлік завершено";
    clearInterval(interval);
  }
}, 1000);
